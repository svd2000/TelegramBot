import telebot
import numpy as np

bot = telebot.TeleBot('5079187403:AAFLBOdPSk1uGw7-9DTAqsOekfgLV32vPyA')

keyboard1 = telebot.types.ReplyKeyboardMarkup()
keyboard1.row('/help','/kript')

keyboard2 = telebot.types.ReplyKeyboardMarkup()
keyboard2.row('/1','/2','/3','/4','/5','/6','/7','/8')

keyboard_nazad = telebot.types.ReplyKeyboardMarkup()
keyboard_nazad.row('назад')

keyboard_1 = telebot.types.ReplyKeyboardMarkup()
keyboard_1.row('/atbashh','/caes','/polibia')

keyboard_1shifrcaes = telebot.types.ReplyKeyboardMarkup()
keyboard_1shifrcaes.row('/caesarshifr','/caesarrasshifr')

keyboard_1shifrpolib = telebot.types.ReplyKeyboardMarkup()
keyboard_1shifrpolib.row('/polibiashifr','/polibiarasshifr')

@bot.message_handler(content_types=['sticker'])
def start_message(message):
    print(message)

@bot.message_handler(commands=['start'])
def start_message(message):
    bot.send_message(message.chat.id, 'Здравствуйте! \n'
                                      'Это бот криптограф, который поможет вам расшифровать и заширровать некоторые сообщения \n'
                                      'Для того что бы посмотреть все доступные шифры нажмите - /kript\n'
                                      'Для ознакомления с командами данного бота пропишите команду /help'.format(name=message.text), reply_markup=keyboard1)

@bot.message_handler(commands=['help'])
def help_message(message):
    bot.send_message(message.chat.id, 'Команды, которые помогут вам взаимодействовать с ботом: \n'
                                      '/start - начало работы \n'
                                      '/kript - выбор шифра'  )

@bot.message_handler(commands=['kript'])
def kript_message(message):
    bot.send_message(message.chat.id,'Выберите шифр:\n'
                                     '/1 - Шифрование шифрами однозначной замены \n'
                                     '/2 - Шифрование шифрами многозначной замены \n'
                                     '/3 - Шифры блочной заменыЗадание \n'
                                     '/4 - Шифры перестановкиЗадание \n'
                                     '/5 - Шифрование шифрами гаммированияЗадание \n'
                                     '/6 - Поточные шифрыЗадание \n'
                                     '/7 - КОМБИНАЦИОННЫЕ ШИФРЫЗадание \n'
                                     '/8 - Асимметричные шифрыЗадание \n'
                                     '/9 - Генерация цифровой подписи \n'
                                     '/10 - ГОСТ Р 34.10 \n'
                                     '/11 - Обмен ключами по алгоритму Diffie–Hellman', reply_markup=keyboard2)



######################################################################################################################################################################################## - 1

# /1 - Шифрование шифрами однозначной замены
@bot.message_handler(commands=['1'])
def one_message(message):
    bot.send_message(message.chat.id, 'Выберите шифр: \n'
                                      'АТБАШ - /atbashh \n'
                                      'Цезарь -/caes \n'
                                      'Шифр Полибия - /polibia',reply_markup=keyboard_1)

#АТБАШ
@bot.message_handler(commands=['atbashh'])
def atb_message(message):
    sent = bot.send_message(message.chat.id,'Введите текст сообщения для зашифрования или расшифрования:')
    bot.register_next_step_handler(sent, atbash)

def atbash(message):
    text = message.text
    text = text.upper().replace(" ", "").replace(".", "ТЧК").replace(
        ",", "ЗПТ")
    abc = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    bot.send_message(message.chat.id, {text.translate(str.maketrans(abc + abc.upper(), abc[::-1] + abc.upper()[::-1]))},reply_markup=keyboard_nazad)
    # print(text.translate(str.maketrans(abc + abc.upper(), abc[::-1] + abc.upper()[::-1])))


#ЦЕЗАРЬ
@bot.message_handler(commands=['caes'])
def cae_message(message):
     bot.send_message(message.chat.id, 'Зашифровать - /caesarshifr или расшифровать - /caesarrasshifr ?',reply_markup=keyboard_1shifrcaes)

@bot.message_handler(commands=['caesarshifr'])
def cae_message1(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения и номер шага:')
    bot.register_next_step_handler(sent, caesar1)

def caesar1(message):
    #ЗАШИФРОВКА
    text = message.text

    text = text.upper().replace(" ", "").replace(".", "ТЧК").replace(",", "ЗПТ")
    smesh = text[-1]
    text = text[ :-1]
    smeshenie = int(smesh)  # Создаем переменную с шагом шифровки
    abc = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    itog = ""  # создаем переменную для вывода итогового сообщения
    for i in text:
        mesto = abc.find(i)  # Вычисляем места символов в списке
        new_mesto = mesto + smeshenie  # Сдвигаем символы на указанный в переменной smeshenie шаг
        if new_mesto > 32:
            new_mesto = new_mesto - 33
        else:
            if i in abc:
                itog += abc[int(new_mesto)]  # Задаем значения в итог
            else:
                itog += i
    text = str(itog)
    bot.send_message(message.chat.id, 'Зашифрованное сообщение: ' + text,reply_markup=keyboard_nazad)
    # bot.send_message(message.chat.id,{text})



@bot.message_handler(commands=['caesarrasshifr'])
def cae_message2(message):
        sent = bot.send_message(message.chat.id, 'Введите текст сообщения и номер шагаы:')
        bot.register_next_step_handler(sent, caesar2)

def caesar2(message):

    text = message.text

    text = text.upper().replace(" ", "")
    smesh = text[-1]
    text = text[:-1]
    smeshenie = int(smesh)  # Создаем переменную с шагом шифровки
    abc = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    itog = ""

    for i in text:
        mesto = abc.find(i)  # Вычисляем места символов в списке
        new_mesto = mesto - smeshenie  # Сдвигаем символы на указанный в переменной smeshenie шаг

        if i in abc:
            itog += abc[new_mesto]  # Задаем значения в итог
        else:
            itog += i
    text = str(itog)
    bot.send_message(message.chat.id,'Расшифрованное сообщение: ' + text,reply_markup=keyboard_nazad)




#ШИФР ПОЛИБИЯ (расшифровка, зашифровка)
@bot.message_handler(commands=['polibia'])
def pol_message(message):
     bot.send_message(message.chat.id, 'Зашифровать - /polibiashifr или расшифровать - /polibiarasshifr ?',reply_markup=keyboard_1shifrpolib)


@bot.message_handler(commands=['polibiashifr'])
def polshifr_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения для шифрования:')
    bot.register_next_step_handler(sent, polibi)

def polibi(message):
    new_text = message.text
    new_text = new_text.upper().replace(" ", "").replace(".", "ТЧК").replace(
        ",", "ЗПТ")
    keys = {"А": "11", "Б": "12", "В": "13",
            "Г": "14", "Д": "15", "Е": "16", "Ё": "21",
            "Ж": "22", "З": "23", "И": "24", "Й": "25",
            "К": "26", "Л": "31", "М": "32", "Н": "33",
            "О": "34", "П": "35", "Р": "36", "С": "41",
            "Т": "42", "У": "43", "Ф": "44", "Х": "45",
            "Ц": "46", "Ч": "51", "Ш": "52", "Щ": "53",
            "Ъ": "54", "Ы": "55", "Ь": "56", "Э": "61",
            "Ю": "62", "Я": "63"
            }
    text = ""
    for i in new_text:
        if i in keys:
            text += keys[i]
            text += " "
    bot.send_message(message.chat.id,'Зашифрованное сообщение: ' + text,reply_markup=keyboard_nazad)

@bot.message_handler(commands=['polibiarasshifr'])
def polshifr2_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения для шифрования:')
    bot.register_next_step_handler(sent, polibi2)

def polibi2(message):
    new_text = message.text
    keys = {"А": "11", "Б": "12", "В": "13",
            "Г": "14", "Д": "15", "Е": "16", "Ё": "21",
            "Ж": "22", "З": "23", "И": "24", "Й": "25",
            "К": "26", "Л": "31", "М": "32", "Н": "33",
            "О": "34", "П": "35", "Р": "36", "С": "41",
            "Т": "42", "У": "43", "Ф": "44", "Х": "45",
            "Ц": "46", "Ч": "51", "Ш": "52", "Щ": "53",
            "Ъ": "54", "Ы": "55", "Ь": "56", "Э": "61",
            "Ю": "62", "Я": "63"}
    text = ""
    decrypt = ""
    for i in new_text:
        if i != " ":
            text += i
        else:
            for j in keys:
                if keys[j] == text:
                    decrypt += j
            text = ""
    bot.send_message(message.chat.id,'Расшифрованное сообщение: ' + decrypt,reply_markup=keyboard_nazad)

######################################################################################################################################################################################## - 2

#/2 - Шифрование шифрами многозначной замены

@bot.message_handler(commands=['2'])
def one_message(message):
    bot.send_message(message.chat.id, 'Выберите шифр: \n'
                                      'Шифр Тритемия - /tritemi \n'
                                      'Шифр Белазо -/belaz \n'
                                      'Шифр Виженера - /vijer')

#Шифр Тритемия
@bot.message_handler(commands=['tritemi'])
def tritemi_message(message):
    bot.send_message(message.chat.id, 'Зашифровать - /tritemshifr или расшифровать - /tritemrasshifr')


@bot.message_handler(commands=['tritemshifr'])
def tritemshifr_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения для шифрования:')
    bot.register_next_step_handler(sent, tritem)

def tritem(message):
    new_text = message.text
    key = "БВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯA"
    key2 = {"А": "0", "Б": "1", "В": "2",
            "Г": "3", "Д": "4", "Е": "5", "Ё": "6",
            "Ж": "7", "З": "8", "И": "9", "Й": "10",
            "К": "11", "Л": "12", "М": "13", "Н": "14",
            "О": "15", "П": "16", "Р": "17", "С": "18",
            "Т": "19", "У": "20", "Ф": "21", "Х": "22",
            "Ц": "23", "Ч": "24", "Ш": "25", "Щ": "26",
            "Ъ": "27", "Ы": "28", "Ь": "29", "Э": "30",
            "Ю": "31", "Я": "32"
            }
    new_text = new_text.upper().replace(" ", "").replace(".", "ТЧК").replace(",","ЗПТ").replace("-", "ТИР")

    text = new_text[0]

    for i in new_text[1:]:
        text += key[int(key2[i])]
        a = 0
        while a < 1:
            key = key[1:] + key[0]
            a = 1
    bot.send_message(message.chat.id,'Зашифрованное сообщение: ' + text,reply_markup=keyboard_nazad)


@bot.message_handler(commands=['tritemrasshifr'])
def tritemshifr_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения для расшифрования:')
    bot.register_next_step_handler(sent, tritem2)

def tritem2(message):
    new_text = message.text
    key_org = "AБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    key = "БВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯA"
    key2 = {"А": "0", "Б": "1", "В": "2",
            "Г": "3", "Д": "4", "Е": "5", "Ё": "6",
            "Ж": "7", "З": "8", "И": "9", "Й": "10",
            "К": "11", "Л": "12", "М": "13", "Н": "14",
            "О": "15", "П": "16", "Р": "17", "С": "18",
            "Т": "19", "У": "20", "Ф": "21", "Х": "22",
            "Ц": "23", "Ч": "24", "Ш": "25", "Щ": "26",
            "Ъ": "27", "Ы": "28", "Ь": "29", "Э": "30",
            "Ю": "31", "Я": "32"
            }
    new_text = new_text.upper().replace(" ", "")

    text = new_text[0]

    for i in new_text[1:]:
        text += key_org[int(key.find(i))]
        a = 0
        while a < 1:
            key = key[1:] + key[0]
            a = 1
    bot.send_message(message.chat.id,'Расшифрованное сообщение: ' + text,reply_markup=keyboard_nazad)


#Шифр Белазо
@bot.message_handler(commands=['belaz'])
def belaz_message(message):
    bot.send_message(message.chat.id, 'Зашифровать - /belazshifr или расшифровать - /belazrasshifr')

@bot.message_handler(commands=['belazshifr'])
def belazshifr_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения и ключ:')
    bot.register_next_step_handler(sent, belaz1)


def belaz1 (message):
    new_text = message.text
    new_text = new_text.upper().replace(" ", "").replace(".", "ТЧК").replace(",","ЗПТ").replace("-", "ТИР")
    key = new_text[-1]
    new_text = new_text[ :-1]
    key = key.upper().replace(" ", "")
    abc = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"

    mass = []
    mass_key = []
    key_mass = []
    itog = []
    text = " "

    for i in key:
        mass_key.append(abc.find(i))
    for i in new_text:
        mass.append(abc.find(i))

    b = len(new_text)
    a = 0

    while a <= b - 1:
        for i in range(0, len(mass_key)):
            if a <= b:
                key_mass.append(int(mass_key[i]))
                a += 1
            else:
                break
        i = 0

    for i in range(0, len(mass)):
        if int(mass[i] + key_mass[i]) > 32:
            itog.append(mass[i] + key_mass[i] - 32)
            text += abc[int(itog[i])]
        else:
            itog.append(mass[i] + key_mass[i])
            text += abc[int(itog[i])]

    bot.send_message(message.chat.id,'Зашифрованное сообщение: ' + text,reply_markup=keyboard_nazad)


@bot.message_handler(commands=['belazrasshifr'])
def belazshifr_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения и ключ:')
    bot.register_next_step_handler(sent, belaz2)

def belaz2 (message):
    new_text = message.text
    new_text = new_text.upper().replace(" ", "").replace(".", "ТЧК").replace(",","ЗПТ").replace("-", "ТИР")
    key = new_text[-1]
    new_text = new_text[ :-1]
    key = key.upper().replace(" ", "")
    abc = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"

    mass = []
    mass_key = []
    key_mass = []
    itog = []
    text = " "

    for i in key:
        mass_key.append(abc.find(i))
    for i in new_text:
        mass.append(abc.find(i))

    b = len(new_text)
    a = 0

    while a <= b - 1:
        for i in range(0, len(mass_key)):
            if a <= b:
                key_mass.append(int(mass_key[i]))
                a += 1
            else:
                break


    for i in range(0,len(mass)):
        if int(mass[i] - key_mass[i]) < 0:
            itog.append(mass[i] - key_mass[i] + 32 )
            text += abc[int(itog[i])]
        else:
            itog.append(mass[i] - key_mass[i])
            text += abc[int(itog[i])]
    bot.send_message(message.chat.id,'Расшифрованное сообщение: ' + text,reply_markup=keyboard_nazad)


#Шифр Виженера
@bot.message_handler(commands=['vijer'])
def belaz_message(message):
    bot.send_message(message.chat.id, 'Зашифровать - /vijershifr или расшифровать - /vijerrasshifr')

@bot.message_handler(commands=['vijershifr'])
def belazshifr_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения и ключ:')
    bot.register_next_step_handler(sent, vijer1)

def vijer1(message):

    new_text = message.text
    new_text = new_text.upper().replace(" ", "").replace(".", "ТЧК").replace(",", "ЗПТ").replace("-", "ТИР")
    key = new_text[-1]
    new_text = new_text[:-1]
    key = key.upper().replace(" ", "")
    abc = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"


    mass_key = []
    mass = []
    key_mass = []
    text = " "
    itog = []

    key += new_text[:-1]

    for i in key:
        mass_key.append(abc.find(i))
    for i in new_text:
        mass.append(abc.find(i))

    b = len(new_text)
    a = 0

    while a <= b - 1:
        for i in range(0, len(mass_key)):
            if a <= b:
                key_mass.append(int(mass_key[i]))
                a += 1
            else:
                break

    for i in range(0, len(mass)):
        if int(mass[i] + key_mass[i]) > 32:
            itog.append(mass[i] + key_mass[i] - 32)
            text += abc[int(itog[i])]
        else:
            itog.append(mass[i] + key_mass[i])
            text += abc[int(itog[i])]

    bot.send_message(message.chat.id, 'Зашифрованное сообщение: '  + text,reply_markup=keyboard_nazad)


@bot.message_handler(commands=['vijerrasshifr'])
def belazshifr_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения и ключ:')
    bot.register_next_step_handler(sent, vijer2)


def vijer2(message):

    new_text = message.text
    new_text = new_text.upper().replace(" ", "").replace(".", "ТЧК").replace(",", "ЗПТ").replace("-", "ТИР")
    key = new_text[-1]
    new_text = new_text[:-1]
    key = key.upper().replace(" ", "")
    abc = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"

    mass=[]
    mass_key =[]
    itog=[]
    text = " "

    for i in key:
        mass_key.append(abc.find(i))
    for i in new_text:
        mass.append(abc.find(i))


    for i in range(0, len(mass)):
        if (mass[i]-mass_key[0])<0:
            itog.append(mass[i]-mass_key[0]+32)
            mass_key[0] = itog[i]
            text +=abc[int(itog[i])]
        else:
            itog.append(mass[i] - mass_key[0])
            mass_key[0] = itog[i]
            text += abc[int(itog[i])]

    bot.send_message(message.chat.id,'Расшифрованное сообщение: ' + text,reply_markup=keyboard_nazad)



######################################################################################################################################################################################## - 3

#3 Шифры блочной замены

@bot.message_handler(commands=['3'])
def one_message(message):
    bot.send_message(message.chat.id, 'Выберите шифр: \n'
                                      'Матричный шифр  - /matrix \n'
                                      'Шифр Плэйфера -/plaef')

@bot.message_handler(commands=['matrix'])
def matrix_message(message):
    bot.send_message(message.chat.id, 'Зашифровать - /matrixshifr или расшифровать - /matrixrasshifr')


@bot.message_handler(commands=['matrixshifr'])
def matrixshifr_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения для шифрования:')
    bot.register_next_step_handler(sent, matrixC)

def matrixC(message):
    alph = '"АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"'
    # Форматируем текст
    new_text = message.text
    new_text = new_text.upper().replace(" ", "").replace(".", "ТЧК").replace(",", "ЗПТ").replace("-", "ТИР")
    R = int(new_text[-1])
    text = new_text[:-1]
    print(R,text)

    # if ((R < 3) & (R != 0)):
    #     return 'Минимальный размер матрицы - 3x3'
    # elif (R != 0):
    #     print("Введите элементы матрицы (разделенные пробелом): ")
    #     x = R * R
    #     bot.send_message(message.chat.id, 'Введите', {x}, 'элементов матрицы (разделенные пробелом):')
    #     # Преобразуем введенные элементы в список, для дальнейшей трансформации в матрицу.
    #     new_text = message.text
    #     entries = list(map(int, new_text.split()))
    #
    #     key = np.array(entries).reshape(R, R)
    #     print(key)
    #
    #     # Проверяем матрицу.
    #
    #     if np.linalg.cond(key) >= 1 / np.finfo(np.float64).eps:
    #         return 'Неподходящая матрица, попробуйте ещё раз'
    # В случае пустого ввода.
    if (R == 0):
        key = np.array([[1, 4, 8], [3, 7, 2], [6, 9, 5]])
    # Объявляем переменную для хранения числовой последовательности.
    # print(key)
    T = []
    # Переводим слова в числовую последовательность.
    for k in range(0, len(text)):
        T.append(alph.find(text[k]) + 1)
    T = np.array(T)
    # print(text, T)
    C = []
    # Создаем векторы отталкиваясь от размеров матрицы (если 3 на 3, то вектор 1 на 3, если 4 на 4, то 1 на 4 и т.п.).
    for vector in np.split(T, len(T) // np.size(key, 0)):
        print(vector)
        C.append(key @ vector)
    print(np.concatenate(C, axis=None), sep='\n')
    # bot.send_message(message.chat.id,'Зашифрованное сообщение: ' + str(np.concatenate(C, axis=None)),reply_markup=keyboard_nazad)


@bot.message_handler(commands=['matrixrasshifr'])
def matrixshifr_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения для шифрования:')
    bot.register_next_step_handler(sent, matrixD)


def matrixD(message):
    alph = '"АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"'
    # Форматируем текст
    new_text = message.text
    new_text = new_text.replace(" ", "")
    R = int(new_text[-1])
    text = new_text[:-1]

    Ti = str.split(new_text.replace('[', '').replace(']', ''), ', ')
    for r in range(0, len(Ti)):
        Ti[r] = int(Ti[r])
    Ti = np.array(Ti)
    # Работаем с ключом

        ## Просим у пользователя число строк и столбцов, заодно сразу и уберем фактор ошибки.

    # if ((R < 3) & (R != -1)):
    #     return 'Минимальный размер матрицы - 3x3'
    # elif (R != -1):
    #     print("Введите элементы матрицы (разделенные пробелом): ")
    #     # Преобразуем введенные элементы в список, для дальнейшей трансформации в матрицу.
    #     entries = list(map(int, input().split()))
    #
    #     key = np.array(entries).reshape(R, R)
    #     print(key)

        # Значения по умолчанию
    if (R == 0):
        key = np.array([[1, 4, 8], [3, 7, 2], [6, 9, 5]])


    # Смотрим на обратную матрицу от key.
    key_inv = np.linalg.inv(key)
    print(key_inv)

    # Объявляем переменную, куда будем складывать результат.
    B = []
    # Возвращаем исходные векторы.
    for vector in np.split(Ti, Ti.size // np.size(key, 0)):
        print(vector)
        B.append(key_inv @ vector)
        # print(B)
    print(np.concatenate(B, axis=None), sep='\n')
    bot.send_message(message.chat.id,'Расшифрованное сообщение: ' + str(np.concatenate(B, axis=None)),reply_markup=keyboard_nazad)

######################################################################################################################################################################################## - 4
#4 Шифры перестановки
@bot.message_handler(commands=['4'])
def one_message(message):
    bot.send_message(message.chat.id, 'Выберите шифр: \n'
                                      'Шифр Решетка Кардано -/kardano \n'
                                      'Шифр вертикальной перестановки - /vert_perestan')

#Решетка кардано
@bot.message_handler(commands=['kardano'])
def kardano_message(message):
    bot.send_message(message.chat.id, 'Зашифровать - /kardanoshifr или расшифровать - /kardanorasshifr')


@bot.message_handler(commands=['kardanoshifr'])
def kardanoshif_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения для шифрования:')
    bot.register_next_step_handler(sent, kardanoE)

def kardanoE(message):
    text = message.text

    def to_indexes(text, abc):
        return [abc.index(symbol) for symbol in clear_text(text, abc)]

    def to_symbols(nums, abc):
        return "".join([abc[num] for num in nums])

    def clear_text(text, abc):
        return "".join([symbol for symbol in text.lower() if symbol in abc])

    abc = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
    grid_kardano = [
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 0, 0, 0, 1, 0, 1, 1, 0, 0],
        [0, 1, 0, 0, 0, 1, 0, 0, 0, 1],
        [0, 0, 0, 1, 0, 0, 0, 1, 0, 0],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 1, 1, 0, 0, 1],
    ]

    def paste_values(text, template_grid, grid):
        indexes = np.argwhere(template_grid == 1)

        for row, col in indexes:
            grid[row][col] = text[0] if len(text) > 0 else "а"
            text = text[1:]
        return grid, text

    def get_templates(grid):
        templates = [
            grid,
            np.rot90(grid, 2),
            np.flip(grid, axis=0),
            np.rot90(np.flip(grid, axis=0), 2),
        ]
        return templates

    def fill_grid(text, template_grid, grid):

        for template in get_templates(template_grid):
            grid, text = paste_values(text, template, grid)

        return grid

    def enc(text, abc=abc, template_grid=grid_kardano, **kwargs):
        text = clear_text(text, abc)
        template_grid = np.array(template_grid)

        result = np.full(template_grid.shape, " ")

        result = fill_grid(text, template_grid, result)
        result = " ".join(["".join(row) for row in result])

        return result

    enc_ = enc(text)
    # print(enc_)
    bot.send_message(message.chat.id, 'Зашифрованное сообщение: ' + enc_,reply_markup=keyboard_nazad)

@bot.message_handler(commands=['kardanorasshifr'])
def kardanorasshifr_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения для расшифрования:')
    bot.register_next_step_handler(sent, kardanoD)

def kardanoD(message):
    text = message.text

    def to_indexes(text, abc):
        return [abc.index(symbol) for symbol in clear_text(text, abc)]

    def to_symbols(nums, abc):
        return "".join([abc[num] for num in nums])

    def clear_text(text, abc):
        return "".join([symbol for symbol in text.lower() if symbol in abc])

    abc = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"
    grid_kardano = [
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        [1, 0, 0, 0, 1, 0, 1, 1, 0, 0],
        [0, 1, 0, 0, 0, 1, 0, 0, 0, 1],
        [0, 0, 0, 1, 0, 0, 0, 1, 0, 0],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 1, 1, 0, 0, 1],
    ]

    def paste_values(text, template_grid, grid):
        indexes = np.argwhere(template_grid == 1)

        for row, col in indexes:
            grid[row][col] = text[0] if len(text) > 0 else "а"
            text = text[1:]
        return grid, text

    def get_templates(grid):
        templates = [
            grid,
            np.rot90(grid, 2),
            np.flip(grid, axis=0),
            np.rot90(np.flip(grid, axis=0), 2),
        ]
        return templates

    def fill_grid(text, template_grid, grid):

        for template in get_templates(template_grid):
            grid, text = paste_values(text, template, grid)

        return grid



    def get_text(grid, template_grid):
        indexes = np.argwhere(template_grid == 1)
        text = ""
        for row, col in indexes:
            text += grid[row][col]
        return text

    def dec(grid, abc=abc, template_grid=grid_kardano, **kwargs):
        grid = [list(row) for row in grid.split()]
        template_grid = np.array(template_grid)
        result = ""
        for template in get_templates(template_grid):
            result += get_text(grid, template)

    dec_ = dec(text)
    bot.send_message(message.chat.id, 'Расшифрованное сообщение: ' + str(dec_), reply_markup=keyboard_nazad)



#Шифр вертикальной перестановки
@bot.message_handler(commands=['vert_perestan'])
def vert_perestan_message(message):
    bot.send_message(message.chat.id, 'Зашифровать - /vert_perestanshifr или расшифровать - /vert_perestanrasshifr')


@bot.message_handler(commands=['vert_perestanshifr'])
def vert_perestanshifr_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения для шифрования:')
    bot.register_next_step_handler(sent, vert_peresE)


def vert_peresE(message):
    import numpy as np
    def to_indexes(text, abc):
        return [abc.index(symbol) for symbol in clear_text(text, abc)]

    # def to_symbols(nums, abc):
    #     return "".join([abc[num] for num in nums])

    def clear_text(text, abc):
        return "".join([symbol for symbol in text.lower() if symbol in abc])

    abc = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"

    def reverse_rows(matrix):
        for i in range(1, matrix.shape[0], 2):
            matrix[i] = np.flip(matrix[i])
        return matrix

    def get_text_matrix(text, cols):
        fill = cols - (len(text) % cols) if (len(text) % cols) != 0 else 0
        text = list(text) + ([''] * fill)
        matrix = np.array(text).reshape(-1, cols)

        return reverse_rows(matrix)

    def get_key(key, abc):
        key = to_indexes(key, abc)
        key = [sorted(key).index(i) for i in key]

        return key

    def enc(text, abc=abc, key="амуры"):
        cols = len(key)
        text = clear_text(text, abc)
        text_matrix = get_text_matrix(text, cols)

        key = get_key(key, abc)

        result = ["".join(text_matrix[:, i]) for i in key]
        return "".join(result)

    text = message.text
    enc_ = enc(text)
    bot.send_message(message.chat.id, 'Зашифрованное сообщение: ' + enc_, reply_markup=keyboard_nazad)


@bot.message_handler(commands=['vert_perestanrasshifr'])
def vert_perestanrasshifr_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения для расшифрования:')
    bot.register_next_step_handler(sent, vert_peresD)


def vert_peresD(message):
    import numpy as np
    def to_indexes(text, abc):
        return [abc.index(symbol) for symbol in clear_text(text, abc)]

    def to_symbols(nums, abc):
        return "".join([abc[num] for num in nums])

    def clear_text(text, abc):
        return "".join([symbol for symbol in text.lower() if symbol in abc])

    abc = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"

    def reverse_rows(matrix):
        for i in range(1, matrix.shape[0], 2):
            matrix[i] = np.flip(matrix[i])
        return matrix

    def get_text_matrix(text, cols):
        fill = cols - (len(text) % cols) if (len(text) % cols) != 0 else 0
        text = list(text) + ([''] * fill)
        matrix = np.array(text).reshape(-1, cols)

        return reverse_rows(matrix)

    def get_key(key, abc):

        key = to_indexes(key, abc)
        key = [sorted(key).index(i) for i in key]

        return key



    def dec(text, abc=abc, key="амуры"):
        text = clear_text(text, abc)
        cols = len(key)
        rows = len(text) // cols
        text_matrix = get_text_matrix(text, rows).transpose()
        # print(text_matrix)

        key = get_key(key, abc)

        result = np.empty(text_matrix.shape, dtype=str)

        for i, key_i in enumerate(key):
            result[:, key_i] = (
                text_matrix[:, i] if i % 2 == 0 else np.flip(text_matrix[:, i])
            )
        result = reverse_rows(result)
        result = ["".join(item) for item in result]

        return "".join(result)

    text = message.text

    dec_ = dec(text)
    bot.send_message(message.chat.id, 'Расшифрованное сообщение: ' + dec_, reply_markup=keyboard_nazad)

######################################################################################################################################################################################## - 5

#5 Шифрование шифрами гаммирования
@bot.message_handler(commands=['5'])
def one_message(message):
    bot.send_message(message.chat.id, 'Выберите шифр: \n'
                                      'Одноразовый блокнот К.Шеннона -/shenon \n'
                                      'Гаммирование ГОСТ 28147-89 - ')

#Shenon
@bot.message_handler(commands=['shenon'])
def shenon_message(message):
    bot.send_message(message.chat.id, 'Зашифровать - /shenonshifr или расшифровать - /shenonrasshifr')


@bot.message_handler(commands=['shenonshifr'])
def kardanoshif_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения для шифрования, сам ключ и кол-во символов в ключ:')
    bot.register_next_step_handler(sent, shenonshifr)

def shenonshifr(message):
    input_str = message.text.replace(' ', '')
    len1 = (int(input_str[-1]) -1 )
    print(len1)
    text = int(input_str[-len1])
    input_str = input_str[ :-len1]

    input_str = input_str.replace(' ', '').replace(',', 'зпт').replace('.', 'тчк').replace('-', '').replace('«','').replace('»', '').replace('–', '').replace(':', '').replace(';', '').lower()
    a = 312
    c = 13
    abc = 'абвгдежзийклмнопрстуфхцчшщъыьэюя'
    n = int(len(abc))
    print(n)
    len_text = int(len(input_str))

    # print(n)
    def get_number_alf(letter):
        number_a = abc.find(letter) + 1
        return number_a

    def get_letter(number):
        letter = abc[number]
        return letter

    message_code_char = []
    for letter in input_str:
        message_code_char.append(get_number_alf(letter))

        # print(message_code_char)

    psh = []

    for i in range(len_text):
        text1 = (a * text + c) % n
        psh.append(text1)
        text = text1

    # print(psh)

    encr = [(i + j) % n for i, j in zip(message_code_char, psh)]
    # print(encr)

    str_full_decode = ''
    for i in (encr):
        str_full_decode += str(get_letter(i - 1))
    # print("Зашифрованное сообщение: ", str_full_decode)
    bot.send_message(message.chat.id, 'Зашифрованное сообщение: ' + str_full_decode, reply_markup=keyboard_nazad)


    message_dec_char = []
    for letter in str_full_decode:
        message_dec_char.append(get_number_alf(letter))

    decr = []

    for i, j in zip(message_dec_char, psh):
        if i > j:
            m1 = i - j
            decr.append(m1)
        else:
            m1 = i + n - j
            decr.append(m1)
            # print(decr)

    str_decode = ''
    for i in (decr):
        str_decode += str(get_letter(i - 1))
    bot.send_message(message.chat.id, 'Расшифрованное сообщение:  ' + str_decode[ :-2], reply_markup=keyboard_nazad)
    # print("Расшифрованное сообщение: ", str_decode[ :-2])




######################################################################################################################################################################################## - 6

#6  Поточные шифры
@bot.message_handler(commands=['6'])
def one_message(message):
    bot.send_message(message.chat.id, 'Выберите шифр: \n'
                                      'А5 /1 - /a51\n'
                                      'А5 /2 - /a52 ')

#А5 /1
@bot.message_handler(commands=['a51'])
def shenon_message(message):
    bot.send_message(message.chat.id, 'Зашифровать - /a51shifr или расшифровать - /a51rasshifr')


@bot.message_handler(commands=['a51shifr'])
def kardanoshif_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения для шифрования, сам ключ и кол-во символов в ключ:')
    bot.register_next_step_handler(sent, shenonshifr)




######################################################################################################################################################################################## - 7

#7  КОМБИНАЦИОННЫЕ ШИФРЫ
@bot.message_handler(commands=['7'])
def one_message(message):
    bot.send_message(message.chat.id, 'Выберите шифр: \n'
                                      'МАГМА - /  \n'
                                      'КУЗНЕЧИК- / \n'
                                      'AES - ')

#А5 /1
@bot.message_handler(commands=['a51'])
def shenon_message(message):
    bot.send_message(message.chat.id, 'Зашифровать - /a51shifr или расшифровать - /a51rasshifr')


@bot.message_handler(commands=['a51shifr'])
def kardanoshif_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения для шифрования, сам ключ и кол-во символов в ключ:')
    bot.register_next_step_handler(sent, shenonshifr)


######################################################################################################################################################################################## - 8

#8 Асимметричные шифры
@bot.message_handler(commands=['8'])
def one_message(message):
    bot.send_message(message.chat.id, 'Выберите шифр: \n'
                                      'RSA - /rsa  \n'
                                      'Elgamal- / \n')

#RSA
@bot.message_handler(commands=['rsa'])
def rsa_message(message):
    bot.send_message(message.chat.id, 'Зашифровать - /rsashifr или расшифровать - /rsarasshifr')


@bot.message_handler(commands=['rsashifr'])
def rsashifr_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения для шифрования, сам ключ и кол-во символов в ключ:')
    bot.register_next_step_handler(sent, rsaE)


def rsaE(message):
    from typing import List

    from os import path

    basedir = path.abspath(path.dirname(__file__))



    alph = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"

    def evklid_gcd(num1, num2):
        while num1 != 0 and num2 != 0:
            if num1 >= num2:
                num1 %= num2
            else:
                num2 %= num1
        return num1 or num2

    def ex_gcd(a, m):
        d = evklid_gcd(a, m)
        a0, a1 = a, m
        x0, x1 = 1, 0
        y0, y1 = 0, 1

        while a1 != 0:
            q = a0 // a1
            a0, a1 = a1, a0 - a1 * q
            x0, x1 = x1, x0 - x1 * q
            y0, y1 = y1, y0 - y1 * q

        return a0, x0

    REPLACES = {
        ",": "ЗПТ",
        ".": "ТЧК",
        "-": "ТИРЕ",
        ";": "ТЧКИЗПТ",
    }

    def to_indexes(text, alph=alph):
        return [alph.index(symbol) for symbol in text]

    def to_symbols(nums, alph=alph):
        return "".join([alph[num] for num in nums])

    def clear_text(text, alph=alph):
        import re

        text = replace_special(text, alph)
        text = text.lower()
        text = re.sub(f"[^{alph}]", "", text)
        return "".join([symbol for symbol in text.lower() if symbol in alph])

    def replace_special(text, alph=alph, replaces=REPLACES):
        for key, value in replaces.items():
            text = text.replace(key, value)
        return text

    class RSA:
        def __init__(self, p, q, e):
            self.n = p * q
            euler = (p - 1) * (q - 1)  # Функция Эйлера
            self.e = e  # Открытая экспонента
            a, x = ex_gcd(e, euler)
            self.d = (a * x) % euler  # Cекретная экспонента

        def __init__(self, open_key, private_key):
            assert open_key[1] == private_key[1]
            self.open_key = open_key
            self.private_key = private_key

        def __str__(self):
            items = [f"n = {self.n}", f"e = {self.e}", f"d = {self.d}"]
            keys = [
                f"Открытый ключ: {self.open_key}",
                f"Секретный ключ: {self.private_key}",
            ]
            res = ["\n".join(s) for s in [items, keys]]
            return "\n" + "\n".join(res) + "\n"

        @property  # Открытый ключ
        def open_key(self):
            return self.e, self.n

        @open_key.setter
        def open_key(self, key):
            self.n = key[1]
            self.e = key[0]

        @property  # Закрытый ключ
        def private_key(self):
            return self.d, self.n

        @private_key.setter
        def private_key(self, key):
            self.n = key[1]
            self.d = key[0]

        # Шифрование
        def enc(self, m: str):
            m = to_indexes(clear_text(m))
            enc_m = [(symbol ** self.e) % self.n for symbol in m]
            return enc_m

        # Расширвание
        def dec(self, enc_m: List[int]):
            m = [(symbol ** self.d) % self.n for symbol in enc_m]
            m = to_symbols(m)
            return m

    def test_crypt(enc, dec):


        text_test = message.text

        alph = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя"


        def clear_text(text, alph=alph):
            import re

            text = replace_special(text, alph)
            text = text.lower()
            text = re.sub(f"[^{alph}]", "", text)
            return "".join([symbol for symbol in text.lower() if symbol in alph])

        text_test = clear_text(text_test)
        # text_1000 = clear_text(text_1000)



        _enc = enc(text_test)
        bot.send_message(message.chat.id, 'Зашированное сообщение:', _enc, reply_markup=keyboard_nazad)
        print("Зашированное сообщение:", _enc)

        _dec = dec(_enc)
        bot.send_message(message.chat.id, 'Расшифрованное  сообщение:', _dec, reply_markup=keyboard_nazad)
        print("Расшифровка пословицы:", _dec)

        # _enc = enc(text_1000)
        # print_kv("Шифровка 1000", _enc)
        #
        # _dec = dec(_enc)
        # print_kv("Расшифровка 1000", _dec)

    if __name__ == "__main__":
        encrypter = RSA((29, 91), (5, 91))
        test_crypt(encrypter.enc, encrypter.dec)



######################################################################################################################################################################################## - 9
#9 Генерация цифровой подписи
@bot.message_handler(commands=['9'])
def one_message(message):
    bot.send_message(message.chat.id, 'Выберите шифр: \n'
                                      'Цифровая подпись RSA - /podrsa  \n'
                                      'Цифровая подпись Elgamal- / \n')

#RSA
@bot.message_handler(commands=['podrsa'])
def rsa_message(message):
    bot.send_message(message.chat.id, 'Зашифровать - /podrsashifr или расшифровать - /podrsarasshifr')


@bot.message_handler(commands=['podrsashifr'])
def rsashifr_message(message):
    sent = bot.send_message(message.chat.id, 'Введите текст сообщения для шифрования, сам ключ и кол-во символов в ключ:')
    bot.register_next_step_handler(sent, podrsaE)

def podrsaE(massege):
    def alf(s):
        llst = ['а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
                'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я']
        s = s.lower().replace(' ', '')
        for sim in s:
            if sim not in llst:
                if sim == '.':
                    s = s.replace('.', 'тчк')
                elif sim == ',':
                    s = s.replace(',', 'зпт')
                elif sim == 'ё':
                    s = s.replace('ё', 'е')
                elif sim == '0':
                    s = s.replace('0', 'ноль')
                elif sim == '1':
                    s = s.replace('1', 'один')
                elif sim == '2':
                    s = s.replace('2', 'два')
                elif sim == '3':
                    s = s.replace('3', 'три')
                elif sim == '4':
                    s = s.replace('4', 'четыре')
                elif sim == '5':
                    s = s.replace('5', 'пять')
                elif sim == '1':
                    s = s.replace('6', 'шесть')
                elif sim == '2':
                    s = s.replace('7', 'семь')
                elif sim == '3':
                    s = s.replace('8', 'восемь')
                elif sim == '4':
                    s = s.replace('9', 'девять')
                else:
                    s = s.replace(sim, '')
        return s
    def decrsakey(s):
        llst = ['а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
                'х',
                'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я']
        s = alf(s)

        P = 13
        Q = 7
        E = 5

        N = P * Q
        F = (P - 1) * (Q - 1)
        for i in range(10000):
            if i * E % F == 1:
                D = i
                break

        h = 0
        for x in s:
            x = llst.index(x)
            h = (h + x ** 2) % 11
        S = h ** D % N
        return s, S, E, N, D

    def checkrsakey(s, S, E, N):
        llst = ['а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
                'х',
                'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я']
        ho = 0
        for x in s:
            x = llst.index(x)
            ho = (ho + x ** 2) % 11
        h = S ** E % N
        if h == ho:
            print(h, "равен", ho, "\n", 'Цифровая подпись подтверждена')
        else:
            print(h, "не равен", ho, "\n", 'Цифровая подпись НЕ подтверждена')

    ### ElGamal подпись ###
    def hi(n: int) -> int:
        result = n
        i = 2
        while i ** 2 < n:
            while n % i == 0:
                n /= i
                result -= result / i
            i += 1
        if n > 1:
            result -= result / n
        return result

    def deckey(s):
        llst = ['а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
                'х',
                'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я']
        s = alf(s)
        h = 0
        P = 59
        G = 17
        X = 19
        Y = G ** X % P
        m = 4
        K = 3
        A = G ** K % P

        for x in s:
            x = llst.index(x)
            h = (h + x ** 2) % 11

        B = ((h - A * X) * K ** (hi(P - 1) - 1)) % (P - 1)
        return s, Y, A, B, P, G, X

    def checkey(s, y, a, b, p, g):
        llst = ['а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
                'х',
                'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я']
        h = 0
        for x in s:
            x = llst.index(x)
            h = (h + x ** 2) % 11
        A1 = (y ** a * a ** b) % p
        A2 = g ** h % p
        if A1 == A2:
            print(A1, "равен", A2)
            print('Цифровая подпись подтверждена')
        else:
            print(A1, "не равен", A2)
            print('Цифровая подпись не подтверждена')

    ### Поговорка ###
    print("RSA")
    texx = massege.text
    text = decrsakey(texx)
    print("Открытые ключи E=", text[2], ", N=", text[3], "\n", "Закрытый ключ D=", text[4], "\n", text[1])
    checkrsakey(text[0], text[1], text[2], text[3])
    print("")

    print("Elgamal")
    text2 = deckey("Птицы с одинаковым оперением охотно летают вместе.")
    print("Открытый ключ Y=", text2[1], "\n", "Закрытый ключ X=", text2[6], "\n", text2[2], text2[3])
    checkey(text2[0], text2[1], text2[2], text2[3], text2[4], text2[5])
    print("")




######################################################################################################################################################################################## - 10
@bot.message_handler(commands=['10'])
def one_message(message):
    bot.send_message(message.chat.id, 'Выберите шифр: \n'
                                      'ГОСТ Р 34.10-94 - /gost94  \n')

#ГОСТ Р 34.10-94
@bot.message_handler(commands=['gost94'])
def rsa_message(message):
    sent = bot.send_message(message.chat.id, 'Вводить рандомные значения? (да/нет)')
    bot.register_next_step_handler(sent, gost94E)

# @bot.message_handler(commands=['gost94shifr'])
# def rsashifr_message(message):
#     sent = bot.send_message(message.chat.id, 'Вводить рандомные значения? (да/нет)')
#     bot.register_next_step_handler(sent, gost94E)

def gost94E(message):

    import random
    from random import randint
    def pred(s):
        llst = ['а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
                'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я']
        s = s.lower().replace(' ', '')
        for sim in s:
            if sim not in llst:
                if sim == '.':
                    s = s.replace('.', 'тчк')
                elif sim == ',':
                    s = s.replace(',', 'зпт')
                elif sim == '-':
                    s = s.replace('-', 'тире')
                elif sim == 'ё':
                    s = s.replace('ё', 'е')
                elif sim == '0':
                    s = s.replace('0', 'ноль')
                elif sim == '1':
                    s = s.replace('1', 'один')
                elif sim == '2':
                    s = s.replace('2', 'два')
                elif sim == '3':
                    s = s.replace('3', 'три')
                elif sim == '4':
                    s = s.replace('4', 'четыре')
                elif sim == '5':
                    s = s.replace('5', 'пять')
                elif sim == '1':
                    s = s.replace('6', 'шесть')
                elif sim == '2':
                    s = s.replace('7', 'семь')
                elif sim == '3':
                    s = s.replace('8', 'восемь')
                elif sim == '4':
                    s = s.replace('9', 'девять')
                else:
                    s = s.replace(sim, '')
        return s

    ### ГОСТ Р 34.10-94 подпись ###
    def decgost94(s, A):
        llst = ['а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
                'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я']
        s = pred(s)

        P = 59
        Q = 29

        # P = random.randint(1, 1000)
        # Q =  random.randint(1, 1000)
        print(P,Q)

        if A ** Q % P != 1:
            bot.send_message(message.chat.id,"A не проходит проверку",reply_markup=keyboard_nazad)
            # print("A не проходит проверку")
            return (A)
        # X = 13
        X =  random.randint(1, 1000)
        Y = A ** X % P
        K =  random.randint(1, 1000)
        # K = 17
        h = 0
        for x in s:
            x = llst.index(x)
            h = (h + x ** 2) % 11

        R = (A ** K % P) % Q
        S = (X * R + K * h) % Q

        return R, S, Q, A, Y, P, s

    def checkgost94(R, S, Q, A, Y, P, s):
        llst = ['а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф',
                'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я']
        h = 0
        for x in s:
            x = llst.index(x)
            h = (h + x ** 2) % 11
        V = h ** (Q - 2) % Q
        Z1 = (S * V) % Q
        Z2 = ((Q - R) * V) % Q
        U = ((A ** Z1 * Y ** Z2) % P) % Q
        if U == R:
            bot.send_message(message.chat.id,str(U) + '\t' +'Равен' + '\t' + str(R) +'\n'+'Цифровая подпись подтверждена', reply_markup=keyboard_nazad)
            # print(U, "равен", R)
            # print('Цифровая подпись подтверждена')
        else:
            bot.send_message(message.chat.id,str(U) + '\t' + 'Не равен' + '\t' + str(R) + '\n' + 'Цифровая подпись не подтверждена',reply_markup=keyboard_nazad)
            # print(U, "не равен", R)
            # print('Цифровая подпись не подтверждена')

    ### Поговорка ###
    itog = decgost94("Птицы с одинаковым оперением охотно летают вместе.", 49)

    # print("ГОСТ Р 34.10-94")
    #
    # print("P =", itog[5])
    # print("Q =", itog[2])
    # print("A =", itog[3])
    # print(itog[0], itog[1])
    bot.send_message(message.chat.id, 'ГОСТ Р 34.10-94 \n P = '+str(itog[5]) +'\n' + 'Q ='+str(itog[2]) + '\n'+'A =' + str(itog[3]), reply_markup=keyboard_nazad)
    checkgost94(itog[0], itog[1], itog[2], itog[3], itog[4], itog[5], itog[6])




######################################################################################################################################################################################## - 11
@bot.message_handler(commands=['11'])
def one_message(message):
    bot.send_message(message.chat.id, 'Выберите шифр: \n'
                                      'Diffie–Hellman - /Diffie  \n'
                     )


# ГОСТ Р 34.10-94
@bot.message_handler(commands=['gost94'])
def rsa_message(message):
    bot.send_message(message.chat.id, 'Зашифровать - /diffieshifr или расшифровать - /diffierasshifr')


@bot.message_handler(commands=['diffieshifr'])
def rsashifr_message(message):
    sent = bot.send_message(message.chat.id,
                            'Введите текст сообщения для шифрования, сам ключ и кол-во символов в ключ:')
    bot.register_next_step_handler(sent, diffieE)

def diffieE(message):
    def dh1():
        N = 46
        A = 15
        K = 19
        Y1 = (A ** K) % N
        return N, A, Y1, K

    def dh2(N, A, Y1):
        K = 16
        Y2 = A ** K % N
        K = Y1 ** K % N
        return Y2, K

    def dhkey(Y2, KU2, N, K1):
        KU1 = Y2 ** K1 % N
        # print("Секретный ключ  пользователя - 1:", K1)
        bot.send_message(message.chat.id, "Секретный ключ  пользователя - 1:" + K1, reply_markup=keyboard_nazad)

        if KU1 == KU2:
            # print("Ключи одинаковые.Общий ключ:", K1)
            bot.send_message(message.chat.id, "Ключи одинаковые.Общий ключ:" + K1, reply_markup=keyboard_nazad)
        else:
            bot.send_message(message.chat.id, "Ключи не совпали", reply_markup=keyboard_nazad)
            # print("Ключи не совпали")

    itog = dh1()
    itog2 = dh2(itog[0], itog[1], itog[2])
    bot.send_message(message.chat.id,'N = '+ str(itog[0]) +"\n"+"A =" + itog[1]+"\n"+"Открытый ключ  пользователя - 1:"+itog[2]+"\n"+"Открытый ключ  пользователя - 2:"+itog2[0]+"\n"+"Секретный ключ  пользователя - 2:" + itog2[1],reply_markup=keyboard_nazad)
    # print("N =", itog[0], "\n", "A =", itog[1], "\n", "Открытый ключ  пользователя - 1:", itog[2], "\n",
    #       "Открытый ключ  пользователя - 2:", itog2[0], "\n", "Секретный ключ  пользователя - 2:", itog2[1])
    dhkey(itog2[0], itog2[1], itog[0], itog[3])














@bot.message_handler(content_types=['text'])
def send_text(message):
    if message.text.lower() == 'привет':
        bot.send_message(message.chat.id, 'Привет, мой друг')
    elif message.text.lower() == 'пока':
        bot.send_message(message.chat.id, 'Прощай')
    elif message.text.lower() == 'я тебя люблю':
        bot.send_sticker(message.chat.id, 'CAACAgIAAxkBAAIF2GGYrluZ4qOeML3KRmt0GQ5KK3QXAALLAANlogMsNAv0zMhL_AciBA')
    elif message.text.lower() == 'назад':
        bot.send_message(message.chat.id, 'Выберите шифр:\n'
                                          '/1 - Шифрование шифрами однозначной замены \n'
                                          '/2 - Шифрование шифрами многозначной замены \n'
                                          '/3 - Шифры блочной заменыЗадание \n'
                                          '/4 - Шифры перестановкиЗадание \n'
                                          '/5 - Шифрование шифрами гаммированияЗадание \n'
                                          '/6 - Поточные шифрыЗадание \n'
                                          '/7 - КОМБИНАЦИОННЫЕ ШИФРЫЗадание \n'
                                          '/8 - Асимметричные шифрыЗадание \n'
                                          '/9 - Генерация цифровой подписи \n'
                                          '/10 - ГОСТ Р 34.10 \n'
                                          '/11 - Обмен ключами по алгоритму Diffie–Hellman', reply_markup=keyboard2)
    else:
        bot.send_message(message.chat.id, 'Такой команды я не понимаю')
        bot.send_sticker(message.chat.id, 'CAACAgIAAxkBAAIF5GGYsuXIwWBUaEOD6eJH6ye6NGRCAAL4AANlogMszW52No-woaAiBA')

@bot.message_handler(content_types=['sticker'])
def sticker_id(message):
    print(message)

bot.polling()

